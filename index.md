# deanturpin/gcc

See the complete documentation on [Docker](https://hub.docker.com/r/deanturpin/gcc) and the output from the [last build](https://deanturpin.gitlab.io/gcc/).

```bash
docker run deanturpin/gcc
```
