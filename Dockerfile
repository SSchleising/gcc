# Start with the latest OS and get it up-to-date
FROM ubuntu:devel AS build
RUN apt update && \
	apt full-upgrade --yes && \
	apt install --yes git make

# Shallow clone the source
RUN git clone --depth=1 git://gcc.gnu.org/git/gcc.git

# Configure the compiler
RUN apt install --yes libgmp3-dev libmpfr-dev libmpc-dev libz-dev flex g++ file python3
RUN mkdir build
WORKDIR build
RUN ../gcc/configure --enable-languages=c++ --disable-multilib --with-system-zlib --disable-bootstrap

# Build and install
RUN make -j $(nproc)
RUN make -j $(nproc) install

# Start next stage and copy the artifacts
FROM ubuntu:devel

# Install the essentials for dev
RUN apt update && \
	apt full-upgrade --yes && \
	apt install --yes \
		git vim curl htop parallel tree tmux cmake locate time neofetch figlet mold entr \
		libgtest-dev libbenchmark-dev libtbb-dev

# Copy build artifacts from first stage
COPY --from=build /usr/local /usr/local

# Dump some version info
WORKDIR /usr/src
CMD \
	figlet -f small docker run deanturpin/gcc && \
	neofetch --stdout && \
	g++ --version && \
	dpkg -l make cmake python3 vim curl bash git mold
